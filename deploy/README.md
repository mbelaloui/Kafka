# Vagrant

This aims of this file is to setup linux instances so we can rapidly have servers for this project

## SetUp ENV

``` vagrant up --provider=virtualbox```

## Clean ENV

``` vagrant destroy -f ```
### NB: Static IP should be carefully choose 

## deploy kafka 

```ansible-playbook -i inventory.yml playbook/main.yml --extra-vars "debug=True"```

## Write some event into kafka topic
```bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092```

## Read Event from kafka topic
```bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092```